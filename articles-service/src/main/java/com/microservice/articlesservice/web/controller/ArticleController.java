package com.microservice.articlesservice.web.controller;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.microservice.articlesservice.dao.ArticleDao;
import com.microservice.articlesservice.model.Article;
import com.microservice.articlesservice.web.exceptions.ArticleGratuitException;
import com.microservice.articlesservice.web.exceptions.ArticleIntrouvableException;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.List;

@RestController
public class ArticleController {

    @Autowired
    private ArticleDao articleDao;
    private static final Logger logger =
            LoggerFactory.getLogger(ArticleController.class);
    @Autowired
    private HttpServletRequest requestContext ;


    //Récupérer la liste des articles
    @RequestMapping(value = "/Articles", method =
            RequestMethod.GET)
    public MappingJacksonValue listeArticles() {
        List<Article> articles = articleDao.findAll();
        SimpleBeanPropertyFilter monFiltre = SimpleBeanPropertyFilter.serializeAllExcept("prixAchat");
        FilterProvider listDeNosFiltres = new SimpleFilterProvider().addFilter("monFiltreDynamique", monFiltre);
        MappingJacksonValue articlesFiltres = new MappingJacksonValue(articles);
        articlesFiltres.setFilters(listDeNosFiltres);
        logger.info("Début d'appel au service Articles pour la requête : " +
                requestContext.getHeader("req-id"));
        return articlesFiltres;
    }

    //Récupérer un article par son Id
    @ApiOperation(value = "Récupère un article grâce à son ID à condition que celui-ci soit en stock!")
    @GetMapping(value="/Articles/{id}")
    public Article afficherUnArticle(@PathVariable int id) {
        Article article = articleDao.findById(id);

        if (article==null) throw new ArticleIntrouvableException("L'article avec l'id " + id + " est INTROUVABLE");
        return article;
    }

    @GetMapping(value = "/test/articles/{prixLimit}")
    public List<Article> testeDeRequetes(@PathVariable int prixLimit) {
        return articleDao.findByPrixGreaterThan(prixLimit);
    }

    @GetMapping(value = "/test/articles/like/{recherche}")
    public List<Article> testeDeRequetes(@PathVariable String
                                                 recherche) {
        return articleDao.findByNomLike("%"+recherche+"%");
    }

    //ajouter un article
    @PostMapping(value = "/Articles")
    public ResponseEntity<Void> ajouterArticle(@RequestBody Article article) {
        Article articleAdded = articleDao.save(article);
        if (articleAdded == null)
            return ResponseEntity.noContent().build();
        // Si le prix est à zéro, on soulève un exception
        if (articleAdded.getPrix() == 0) throw new ArticleGratuitException("L'article avec l'id " + articleAdded.getId() + " à un prix de vente à zéro");
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(articleAdded.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    //Calculer la marge d'un article
    @ApiOperation(value = "Calcule la marge faite sur un article grâce à son ID, à condition que celui-ci soit en stock!")
    @GetMapping(value="/AdminArticles/{id}")
    public int calculerMargeArticle(@PathVariable int id) {
        Article article = articleDao.findById(id);
        if (article==null) throw new ArticleIntrouvableException("L'article avec l'id " + id + " est INTROUVABLE");

        return article.getPrix() - article.getPrixAchat();
    }

    //Afficher les articles par ordre alphabétique
    @ApiOperation(value = "Affiche les articles par ordre alphabétique")
    @RequestMapping(value = "/ArticlesOrderBy", method = RequestMethod.GET)
    public List<Article> trierArticlesParOrdreAlphabetique() {
        logger.info("Début d'appel au service Articles pour la requête : " +
                requestContext.getHeader("req-id"));
        return articleDao.findByOrderByNomAsc();
    }

    // Modifier un article
    @PutMapping (value = "/Articles")
    public void updateArticle(@RequestBody Article article) {
        articleDao.save(article);
    }

    // Supprimer un article
    @DeleteMapping (value = "/Articles/{id}")
    public void supprimerArticle(@PathVariable int id) {
        articleDao.deleteById(id);
    }


}

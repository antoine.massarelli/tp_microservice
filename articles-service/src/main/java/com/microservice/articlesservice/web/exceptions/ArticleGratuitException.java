package com.microservice.articlesservice.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.PARTIAL_CONTENT)
public class ArticleGratuitException extends RuntimeException {
    public ArticleGratuitException(String s) {
        super(s);
    }
}
